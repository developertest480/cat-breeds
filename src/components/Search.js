/* eslint-disable eqeqeq */
import { FormControl, InputGroup, Dropdown, DropdownButton } from "react-bootstrap"
import { BsCheck2 } from "react-icons/bs"

function DropDown({ sortBy, onSortByChange, orderBy, onOrderByChange }) {
    return (
        <>
            <DropdownButton
                variant="secondary"
                title="Sort">
                <Dropdown.Item href="#" onClick={() => onSortByChange('name')}>Name {(sortBy == 'name') && <BsCheck2 className="float-end" />}</Dropdown.Item>
                <Dropdown.Item href="#" onClick={() => onSortByChange('origin')}>Origin {(sortBy == 'origin') && <BsCheck2 className="float-end" />}</Dropdown.Item>
                <Dropdown.Item href="#" onClick={() => onSortByChange('affection_level')}>Affection Level {(sortBy == 'affection_level') && <BsCheck2 className="float-end" />}</Dropdown.Item>
                <Dropdown.Item href="#" onClick={() => onSortByChange('child_friendly')}>Child Friendly {(sortBy == 'child_friendly') && <BsCheck2 className="float-end" />}</Dropdown.Item>
                <Dropdown.Item href="#" onClick={() => onSortByChange('dog_friendly')}>Dog Friendly {(sortBy == 'dog_friendly') && <BsCheck2 className="float-end" />}</Dropdown.Item>
                <Dropdown.Item href="#" onClick={() => onSortByChange('energy_level')}>Energy Level {(sortBy == 'energy_level') && <BsCheck2 className="float-end" />}</Dropdown.Item>
                <Dropdown.Item href="#" onClick={() => onSortByChange('grooming')}>Grooming {(sortBy == 'grooming') && <BsCheck2 className="float-end" />}</Dropdown.Item>
                <Dropdown.Item href="#" onClick={() => onSortByChange('health_issues')}>Health Issues {(sortBy == 'health_issues') && <BsCheck2 className="float-end" />}</Dropdown.Item>
                <Dropdown.Item href="#" onClick={() => onSortByChange('intelligence')}>Intelligence {(sortBy == 'intelligence') && <BsCheck2 className="float-end" />}</Dropdown.Item>
                <Dropdown.Item href="#" onClick={() => onSortByChange('shedding_level')}>Shedding Level {(sortBy == 'shedding_level') && <BsCheck2 className="float-end" />}</Dropdown.Item>
                <Dropdown.Item href="#" onClick={() => onSortByChange('stranger_friendly')}>Stranger Friendly {(sortBy == 'stranger_friendly') && <BsCheck2 className="float-end" />}</Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Item href="#" onClick={() => onOrderByChange('asc')}>ASC {(orderBy == 'aptDate') && <BsCheck2 className="float-end" />}</Dropdown.Item>
                <Dropdown.Item href="#" onClick={() => onOrderByChange('desc')}>DESC {(orderBy == 'aptDate') && <BsCheck2 className="float-end" />}</Dropdown.Item>
            </DropdownButton>
        </>
    )
}

const Search = ({ query, onQueryChange, sortBy, onSortByChange, orderBy, onOrderByChange }) => {
    return (
        <>
            <InputGroup className='mb-3 search-control'>
                <FormControl
                    placeholder='Search'
                    onChange={(event) => {
                        onQueryChange(event.target.value)
                    }} value={query} />
                <DropDown
                    sortBy={sortBy}
                    onSortByChange={mySort => onSortByChange(mySort)}
                    orderBy={orderBy}
                    onOrderByChange={myOrder => onOrderByChange(myOrder)} />
            </InputGroup>
        </>
    )
}

export default Search;