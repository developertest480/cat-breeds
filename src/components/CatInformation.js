import 'bootstrap/dist/css/bootstrap.min.css';
import _ from 'lodash';
import { Button, Col, Container, ListGroup, Row, Nav, Card } from 'react-bootstrap';
import { BsFillStarFill, BsFillBackspaceFill } from 'react-icons/bs';
import { FiExternalLink } from 'react-icons/fi';
import { FaCat } from 'react-icons/fa';
import { React } from 'react';
import { useLocation } from 'react-router-dom';

const CatInformation = ({ cat }) => {

    const location = useLocation();
    const displayCat = location.state.displayCat;

    let [imageId, imageWidth, imageHeight, imageURL] = _.values(displayCat.image);

    let divStyle = {
        backgroundImage: 'url(' + imageURL + ')',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        width: '100%',
        height: '100%'
      };

    const createStars = (arg) => {
        return [...Array(arg)].map((e, i) => <span className='rating' key={i}><BsFillStarFill /></span>)
    }

    return (
        <>
            <Container>
            <Row className='justify-content-center'>
                <Col md='8' className='mt-3'>
                    <h1 className='app-title fw-bolder lato'><FaCat /> Cat Breeds</h1>
                </Col>
            </Row>
                <Row className='justify-content-center'>
                    <Col md="8">
                        <Card className='mb-3'>
                            <Card.Header>
                                <Container>
                                    <Row>
                                        <Col>
                                            <Nav variant="pills" defaultActiveKey="/">
                                                <Nav.Item className="header">
                                                    <h1 className='text-center fw-bolder mt-3 lato'>{displayCat.name}</h1>
                                                </Nav.Item>
                                                <Nav.Item className='back-button'>
                                                    <Nav.Link href="/"><BsFillBackspaceFill /> Back</Nav.Link>
                                                </Nav.Item>
                                            </Nav>
                                        </Col>
                                    </Row>
                                </Container>
                            </Card.Header>
                            <ListGroup variant="flush">
                                <ListGroup.Item>
                                    <Container>
                                        <Row>
                                            <Col md='6' className='mt-3 lato'>
                                                <div className="cat-image" style={divStyle}>
                                                    {/* <img className='mb-3' src={imageURL ? imageURL : './images/cat-placeholder.png'} width='100%' height='auto' alt='cat' /> */}
                                                </div>
                                            </Col>
                                            <Col md='6' className='mt-3 lato'>
                                                <p className='cat-name lato'>Name: {displayCat.name}</p>
                                                <p className='cat-origin lato'>Origin: {displayCat.origin}</p>
                                                <p><strong>Temperament:</strong><br /><small>{displayCat.temperament}</small></p>
                                                <p><strong>Life Span:</strong> {displayCat.life_span}</p>
                                                <Button variant="outline-dark" className='more' onClick={() => window.open(displayCat.wikipedia_url, "_blank")}>Wikipedia < FiExternalLink /></Button>
                                                <p className='mt-4 lato'><small>{displayCat.description}</small></p>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md='4' className='mt-4 lato cat-traits'>
                                                <p><strong>Affection Level:</strong> {createStars(displayCat.affection_level)}</p>
                                                <p><strong>Child Friendly:</strong> {createStars(displayCat.child_friendly)}</p>
                                                <p><strong>Dog Friendly:</strong> {createStars(displayCat.dog_friendly)}</p>
                                                <p><strong>Energy Level:</strong> {createStars(displayCat.energy_level)}</p>
                                                <p><strong>Grooming:</strong> {createStars(displayCat.grooming)}</p>
                                            </Col>
                                            <Col md='4' className='mt-4 lato cat-traits'>
                                                <p><strong>Health Issues:</strong> {createStars(displayCat.health_issues)}</p>
                                                <p><strong>Intelligence:</strong> {createStars(displayCat.intelligence)}</p>
                                                <p><strong>Shedding Level:</strong> {createStars(displayCat.shedding_level)}</p>
                                                <p><strong>Stranger Friendly:</strong> {createStars(displayCat.stranger_friendly)}</p>
                                            </Col>
                                        </Row>
                                    </Container>
                                </ListGroup.Item>
                            </ListGroup>
                            <Card.Footer>&nbsp;</Card.Footer>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </>

    )
}

export default CatInformation;