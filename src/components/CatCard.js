import { React } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, Col, Container, ListGroup, Row } from 'react-bootstrap';
import { BsFillStarFill } from 'react-icons/bs';
import { AiOutlineRead } from 'react-icons/ai';

const CatCard = ({ cat }) => {

    const createStars = (arg) => {
        return [...Array(arg)].map((e, i) => <span className='rating' key={i}><BsFillStarFill /></span>)
    }

    const navigate = useNavigate();
    const handleClick = (data) => {
        navigate('/details', { state: { displayCat: cat } });
    }

    return (
        <>
            <ListGroup.Item>
                <Container>
                    <Row>
                        <Col className='mt-3 lato'>
                            <p className='cat-name lato'>Name: {cat.name}</p>
                            <p className='cat-origin lato'>Origin: {cat.origin}</p>
                            <p><strong>Temperament:</strong><br /><small>{cat.temperament}</small></p>
                            <p><strong>Life Span:</strong> {cat.life_span}</p>
                            <Button variant="outline-dark" className='more' onClick={() => handleClick()}>Learn More  < AiOutlineRead /></Button>
                        </Col>
                        <Col className='mt-4 lato'>
                            <p><strong>Affection Level:</strong> {createStars(cat.affection_level)}</p>
                            <p><strong>Child Friendly:</strong> {createStars(cat.child_friendly)}</p>
                            <p><strong>Dog Friendly:</strong> {createStars(cat.dog_friendly)}</p>
                            <p><strong>Energy Level:</strong> {createStars(cat.energy_level)}</p>
                            <p><strong>Grooming:</strong> {createStars(cat.grooming)}</p>
                            <p><strong>Health Issues:</strong> {createStars(cat.health_issues)}</p>
                            <p><strong>Intelligence:</strong> {createStars(cat.intelligence)}</p>
                            <p><strong>Shedding Level:</strong> {createStars(cat.shedding_level)}</p>
                            <p><strong>Stranger Friendly:</strong> {createStars(cat.stranger_friendly)}</p>
                        </Col>
                    </Row>
                </Container>
            </ListGroup.Item>
        </>

    )
}

export default CatCard;