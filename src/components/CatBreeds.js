import 'bootstrap/dist/css/bootstrap.min.css';
import { FaCat } from 'react-icons/fa';
import { Card, Col, ListGroup, Row, Container } from 'react-bootstrap';
import { React, useCallback, useEffect, useState } from 'react';
import Search from './Search';
import CatCard from './CatCard';

const CatBreeds = () => {

    let [catList, setCatList] = useState([]);
    let [query, setQuery] = useState('');
    let [sortBy, setSortBy] = useState('name');
    let [orderBy, setOrderBy] = useState('asc');

    const filteredCats = catList.filter(
        item => {
            return (
                item.name.toLowerCase().includes(query.toLowerCase()) ||
                item.origin.toLowerCase().includes(query.toLowerCase()) ||
                item.temperament.toLowerCase().includes(query.toLowerCase()) ||
                item.description.toLowerCase().includes(query.toLowerCase())
            )
        }
    ).sort((a, b) => {
        let order = (orderBy === 'asc') ? 1 : -1;
        return (
            a[sortBy] < b[sortBy] ? -1 * order : 1 * order
        )
    })

    const fetchData = useCallback(() => {
        fetch('https://api.thecatapi.com/v1/breeds?attach_breed=0')
            .then(response => response.json())
            .then(data => {
                setCatList(data)
            });
    }, [])

    useEffect(() => {
        fetchData()
    }, [fetchData])

    return (
        <Container>
            <Row className='justify-content-center'>
                <Col md='4' className='mt-3'>
                    <h1 className='app-title fw-bolder lato'><FaCat /> Cat Breeds</h1>
                </Col>
                <Col md='4' className='mt-3 search-container'>
                    <Search
                        query={query}
                        onQueryChange={myQuery => setQuery(myQuery)}
                        orderBy={orderBy}
                        onOrderByChange={mySort => setOrderBy(mySort)}
                        sortBy={sortBy}
                        onSortByChange={mySort => setSortBy(mySort)}
                    />
                </Col>
            </Row>
            <Row className='justify-content-center'>
                <Col md="8">
                    <Card className='mb-3'>
                        <Card.Header>&nbsp;</Card.Header>
                        <ListGroup variant="flush">
                            {filteredCats.map(cat => (
                                <CatCard key={cat.id} cat={cat} />
                            ))}
                        </ListGroup>
                        <Card.Footer>&nbsp;</Card.Footer>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}

export default CatBreeds;