import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import CatBreeds from './components/CatBreeds';
import CatInformation from './components/CatInformation';

function App() {
  return (
    <div>
      <Routes>
        <Route exact path='/' element={< CatBreeds />}></Route>
        <Route exact path='/details' element={< CatInformation />}></Route>
      </Routes>
    </div>
  );
}

export default App;