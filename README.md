# Cat Breeds

This project uses [the Cat API](https://docs.thecatapi.com/).

## Setup / Installation

### Step 1: Download source code

Clone the repo to your local machine.

**git clone https://gitlab.com/developertest480/cat-breeds.git**

### Step 2: Navigate to the builds directory

`cd cat-breeds\build`

### Step 3: Run the application

### `npx serve`

Serves the app on the port 3000.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.
